# laravel graphql demo

Demo endpoint: [get tasks with id and description](http://localhost:8080/graphql?query=query+{tasks{id+description}})  
Workshop in combination with: [vue-apollo_graphql_demo](https://gitlab.com/ikdoeict/public/vakken/full-stack-development/vue-apollo_graphql_demo)

## Running and stopping the Docker MCE

* Run the environment, using Docker, from your terminal/cmd

```shell
cd <your-project>
docker-compose up
```

* Stop the environment in your terminal/cmd by pressing <code>
  Ctrl+C</code>
* In order to avoid conflicts with your lab/slides environment, run
  from your terminal/cmd

```shell
docker-compose down
```

## Further installation of the Laravel project

* Run from your terminal/cmd

```shell
docker-compose exec -u 1000:1000 php-web bash
$ cp .env.example .env
$ composer install
$ php artisan key:generate
$ touch storage/logs/laravel.log
$ chmod 777 -R storage
$ php artisan storage:link
$ php artisan migrate --seed
$ exit
```

