<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Carbon\Carbon;
use Faker\Factory as FakerFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $faker = FakerFactory::create();
        $faker->seed(666);
        $names = ['jan', 'piet', 'joris', 'corneel'];
        foreach($names as $name) {
            DB::table('users')->insert([
                'name' => $name,
                'email' => $name . '@odisee.be',
                'password' => Hash::make('Azerty123'),
                'role' => ($name === 'corneel' ? 'admin' : 'standard'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
        for ($i = 1; $i < 4; $i++) {
            DB::table('tasks')->insert([
                'description' => 'A not so urgent task',
                'priority' => 'low',
                'user_id' => $i,
                'created_at' => $faker->dateTimeThisYear()->format('Y-m-d H:i:s')
            ]);
            DB::table('tasks')->insert([
                'description' => 'An urgent task',
                'priority' => 'medium',
                'user_id' => $i,
                'created_at' => $faker->dateTimeThisYear()->format('Y-m-d H:i:s')
            ]);
            DB::table('tasks')->insert([
                'description' => 'A very urgent task',
                'priority' => 'high',
                'user_id' => $i,
                'created_at' => $faker->dateTimeThisYear()->format('Y-m-d H:i:s')
            ]);
        }


    }
}
