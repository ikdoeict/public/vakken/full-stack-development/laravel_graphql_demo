<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Models\Task;
use Closure;
use GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class TaskQuery extends Query
{
    protected $attributes = [
        'name' => 'tasks',
        'description' => 'A query returning a set of tasks'
    ];

    // http://localhost:8080/graphql?query=query+{tasks{id+description}}
    // http://localhost:8080/graphql?query=query+{tasks(id:+"1"){id+description}}

    public function type(): Type
    {
        return Type::nonNull(Type::listOf(Type::nonNull(GraphQL::type('Task'))));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string(),
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        /** @var SelectFields $fields */
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        $builder = Task::select($select)->with($with);

        if (isset($args['id'])) {
            $builder->where('id', $args['id']);
        }

        return $builder->get();
    }
}
